let elixir = require('laravel-elixir');
require('laravel-elixir-livereload');

elixir.config.assetsPath = 'themes/juzshoplah/assets/';
elixir.config.publicPath = 'themes/juzshoplah/assets/compiled/';

elixir(function(mix){

    mix.sass('style.scss');

    mix.scripts([
        'jquery.js',
        'slick.js',
        'app.js'
    ]);

    mix.livereload([
        'themes/juzshoplah/assets/compiled/css/style.css',
        'themes/juzshoplah/**/*.htm',
        'themes/juzshoplah/assets/compiled/js/*.js'
    ])

});
